# README #

This README documents steps necessary to get your application up and running.

### What is this repository for? ###
## Quick summary ##
KISS PHP MVC is a simple php framework for building web applications. Using pretty url's and simple routing you can build application quickly and
remain Object oriented and develop with expansion in mind.

## Version ##
1.0

### How do I get set up? ###

## Summary of set up ##
Start by forking your own version of this repo.
 Starting with your own fork of the original code will allow you to design the application the way you choose.
 
To begin with lets discuss the directory layout.
<pre>
/Config
</pre>
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact